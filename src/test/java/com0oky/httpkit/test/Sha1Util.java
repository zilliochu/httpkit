/**
 * @author mdc
 * @date 2017年7月5日 下午7:55:51
 */
package com0oky.httpkit.test;

import java.security.MessageDigest;

/**
 * @author mdc
 * @date 2017年7月5日 下午7:55:51
 */
public class Sha1Util {
	/**
	 * 获取sha1加密
	 * @author mdc
	 * @date 2017年7月5日 下午6:51:15
	 * @param str
	 * @return
	 */
	public static String getSha1(String str){
	    if (null == str || 0 == str.length()){
	        return null;
	    }
	    
	    char[] hexDigits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
	    try {
	        MessageDigest mdTemp = MessageDigest.getInstance("SHA1");
	        mdTemp.update(str.getBytes("UTF-8"));
	         
	        byte[] md = mdTemp.digest();
	        int j = md.length;
	        char[] buf = new char[j * 2];
	        int k = 0;
	        
	        for (int i = 0; i < j; i++) {
	            byte byte0 = md[i];
	            buf[k++] = hexDigits[byte0 >>> 4 & 0xf];
	            buf[k++] = hexDigits[byte0 & 0xf];
	        }
	        return new String(buf);
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	    
	    return null;
	}
}
